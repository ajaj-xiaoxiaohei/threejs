/**
 * 该版本threejs中无FontLoader、TextGeometry
 */
import * as THREE from 'three'

export const textGeometry = {
    methods: {
        addAllTexts() {
            this.textOne()
        },
        textOne() {
            const loader = new FontLoader();
            const path = require('../../assets/font/helvetiker_regular.typeface.json')

            loader.load(path, (font) => {

                const geometry = new TextGeometry('Hello three.js!', {
                    font: font,
                    size: 80,
                    height: 5,
                    curveSegments: 12,
                    bevelEnabled: true,
                    bevelThickness: 10,
                    bevelSize: 8,
                    bevelSegments: 5
                });

                const material = new THREE.MeshPhongMaterial({
                    color: 0xff0000
                });
                const mesh = new THREE.Mesh(geometry, material);
                mesh.position.set(0, 900, 0)
                this.add(mesh);
            });
        }
    }
}