import * as THREE from 'three'

export let sprite = {
    methods: {
        addAllSprites() {
            this.addSprites()
        },
        addSprites() {
            var texture = new THREE.TextureLoader().load(require('../../assets/tree.png'));
            // 创建精灵材质对象SpriteMaterial
            var spriteMaterial = new THREE.SpriteMaterial({
                color: 0xff00ff, //设置精灵矩形区域颜色
                // rotation: Math.PI / 4, //旋转精灵对象45度，弧度值
                map: texture, //设置精灵纹理贴图
            });

            for (let i = 0; i < 50; i++) {
                for (let j = 0; j < 22; j++) {
                    // 创建精灵模型对象，不需要几何体geometry参数
                    var sprite = new THREE.Sprite(spriteMaterial);
                    this.add(sprite);
                    // 控制精灵大小，比如可视化中精灵大小表征数据大小
                    sprite.scale.set(100, 100, 1); //// 只需要设置x、y两个分量就可以
                    sprite.position.set(-3000 + i * 125, -115, 900 + j * 100)
                }
            }

            for (let i = 0; i < 50; i++) {
                for (let j = 0; j < 　22; j++) {
                    // 创建精灵模型对象，不需要几何体geometry参数
                    var sprite = new THREE.Sprite(spriteMaterial);
                    this.add(sprite);
                    // 控制精灵大小，比如可视化中精灵大小表征数据大小
                    sprite.scale.set(100, 100, 1); //// 只需要设置x、y两个分量就可以
                    sprite.position.set(-3000 + i * 125, -115, -3000 + j * 100);
                }
            }
        }
    }
}