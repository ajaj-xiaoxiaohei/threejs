import * as THREE from 'three'

// 创建建筑物
export let building = {
    methods: {
        addAllBuildings() {
            this.buildingOne()
            this.buildingTwo()
            this.buildingThree()
            this.buildingFour()
        },
        buildingOne() {
            // 纹理贴图
            let number = 5
            const geometryO = new THREE.BoxBufferGeometry(300, 70, 300);
            let textureLoader = new THREE.TextureLoader();
            textureLoader.load(require('../../assets/wall.jpg'), (texture) => {
                let material = new THREE.MeshLambertMaterial({
                    // color: 0x0000ff,
                    // 设置颜色纹理贴图：Texture对象作为材质map属性的属性值
                    map: texture, //设置颜色贴图属性值
                }); //材质对象Material

                // 物体一
                for (let i = 0; i < number; i++) {
                    let mesh = new THREE.Mesh(geometryO, material);
                    mesh.position.set(-500, -130 + i * 80, -500)
                    this.add(mesh);
                }
            })
        },
        buildingTwo() {
            // 纹理贴图
            let number = 5
            const geometryO = new THREE.BoxBufferGeometry(300, 70, 300);
            let textureLoader = new THREE.TextureLoader();
            textureLoader.load(require('../../assets/wall.jpg'), (texture) => {
                let material = new THREE.MeshLambertMaterial({
                    // color: 0x0000ff,
                    // 设置颜色纹理贴图：Texture对象作为材质map属性的属性值
                    map: texture, //设置颜色贴图属性值
                }); //材质对象Material

                // 物体一
                for (let i = 0; i < number; i++) {
                    let mesh = new THREE.Mesh(geometryO, material);
                    mesh.position.set(500, -130 + i * 80, -500)
                    this.add(mesh);
                }
            })
        },
        buildingThree() {
            // 纹理贴图
            let number = 5
            const geometryO = new THREE.BoxBufferGeometry(300, 70, 300);
            let textureLoader = new THREE.TextureLoader();
            textureLoader.load(require('../../assets/wall.jpg'), (texture) => {
                let material = new THREE.MeshLambertMaterial({
                    // color: 0x0000ff,
                    // 设置颜色纹理贴图：Texture对象作为材质map属性的属性值
                    map: texture, //设置颜色贴图属性值
                }); //材质对象Material

                // 物体一
                for (let i = 0; i < number; i++) {
                    let mesh = new THREE.Mesh(geometryO, material);
                    mesh.position.set(-500, -130 + i * 80, 500)
                    this.add(mesh);
                }
            })
        },

        buildingFour() {
            // 纹理贴图
            let number = 5
            const geometryO = new THREE.BoxBufferGeometry(300, 70, 300);
            let textureLoader = new THREE.TextureLoader();
            textureLoader.load(require('../../assets/wall.jpg'), (texture) => {
                let material = new THREE.MeshLambertMaterial({
                    // color: 0x0000ff,
                    // 设置颜色纹理贴图：Texture对象作为材质map属性的属性值
                    map: texture, //设置颜色贴图属性值
                }); //材质对象Material

                // 物体一
                for (let i = 0; i < number; i++) {
                    let mesh = new THREE.Mesh(geometryO, material);
                    mesh.position.set(500, -130 + i * 80, 500)
                    this.add(mesh);
                }
            })
        }
    }
}