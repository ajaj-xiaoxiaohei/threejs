import * as THREE from 'three'
import { STLLoader } from 'three/examples/jsm/loaders/STLLoader'

export let stl = {
    methods: {
        addAllStls() {
            this.addStlOne()
        },
        addStlOne() {
            // /**
            //  * stl数据加载
            //  */
            // var loader = new STLLoader();
            // // 立方体默认尺寸长宽高各200
            // loader.load(require('../../assets/stl/高模冰墩墩.stl'), (geometry) => {
            //     // 控制台查看加载放回的threejs对象结构
            //     console.log(geometry);
            //     // 查看顶点数，一个立方体6个矩形面，每个矩形面至少2个三角面，每个三角面3个顶点，
            //     // 如果没有索引index复用顶点，就是说一个立方体至少36个顶点
            //     console.log(geometry.attributes.position.count);
            //     // 缩放几何体
            //     // geometry.scale(0.5,0.5,0.5);
            //     // 几何体居中
            //     // geometry.center();
            //     // 平移立方体
            //     // geometry.translate(-50,-50,-50);
            //     var material = new THREE.MeshLambertMaterial({
            //         color: 0x0000ff,
            //     }); //材质对象Material
            //     var mesh = new THREE.Mesh(geometry, material); //网格模型对象Mesh
            //     this.add(mesh); //网格模型添加到场景中
            // })


            // /**
            //  * 点渲染模式
            //  */
            // var loader = new STLLoader();
            // loader.load(require('../../assets/stl/高模冰墩墩.stl'), (geometry) => {
            //     console.log('===geometry===>', geometry)
            //     var material = new THREE.PointsMaterial({
            //         color: 0x000000,
            //         size: 0.5 //点对象像素尺寸
            //     }); //材质对象
            //     var points = new THREE.Points(geometry, material); //点模型对象
            //     this.add(points); //点对象添加到场景中
            // })
        }
    }
}