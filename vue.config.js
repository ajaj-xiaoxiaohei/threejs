const { resolve } = require("path")
const path = require('path')
module.exports = {
    transpileDependencies: true,

    /* 构建输出目录（打包位置） */
    outputDir: 'dist',

    /**
     * 静态资源目录:
     * 放置生成的静态资源（js、css、img、fonts）的目录
     * 
     */
    assetsDir: 'assets',

    /**
     * eslint代码检测:
     * 值：
     * false: 关闭每次保存都进行检测
     * true: 开启每次保存都进行检测，效果与warning一样
     * ‘error’: 开启每次保存都进行检测，lint错误将显示到浏览器页面上，且编译失败
     * ‘default’: 同‘error’
     * ‘warning’: 开启每次保存都进行检测，lint错误将显示到控制台命令行，而且编译并不会失败
     */
    lintOnSave: false,

    /* 注意sass，scss，less的配置 */
    css: {
        loaderOptions: {
            sass: {
                prependData: `
          @import "~@/assets/scss/variables.scss";
        `
            },
            scss: {
                prependData: `@import "~@/assets/scss/variables.scss";`
            },
            less: {
                globalVars: {
                    primary: '#fff'
                }
            }
        },
    },

    // chainWebpack(config) {
    //     config.module.rule("images")
    //         .test(/\.(png|jpeg|jpg)$/)
    //         .use("url-loader")
    //         .loader("url-loader").options({
    //             limit: 1024 * 100, // 小于10k的图片采用baseurl，大于和等于8k的就正常打包成图片
    //             name: "static/[name].[ext]" //图片大于等于10k时，设置打包后图片的存放位置 name是文件名   ext是文件后缀
    //         })
    // },


    configureWebpack: (config) => {
        if (process.env.NODE_ENV === 'production') {
            // 为生产环境修改配置...
            config.mode = 'production'
        } else {
            // 为开发环境修改配置...
            config.mode = 'development'
        }
        //修改title
        // configureWebpack: config => {
        //     config.plugins.forEach((val) => {
        //         if(val instanceof HtmlWebpackPlugin){
        //             val.option.title = '很爱很爱你'
        //         }
        //     })
        // }

        // 开发生产共同配置别名
        Object.assign(config.resolve, {
            alias: {
                '@': path.resolve(__dirname, './src'),
                'assets': path.resolve(__dirname, './src/assets'),
                'static': path.resolve(__dirname, './public/static'),
            }
        })
    },

    /* webpack-dev-server 相关配置 */
    // devServer: {
    //     /* 自动打开浏览器 */
    //     open: true,
    //     /* 设置为0.0.0.0则所有的地址均能访问 */
    //     host: '0.0.0.0',
    //     port: 80,
    //     https: false,
    //     hotOnly: false,
    //     /* 使用代理 */
    //     proxy: {
    //         '/api': {
    //             /* 目标代理服务器地址 */
    //             target: 'https://www.baidu.com/',
    //             /* 允许跨域 */
    //             changeOrigin: true,
    //         },
    //     },
    // }
}